/* Create a simple shipping function that will use switch and if else statements:
1. Declare a function that will be named as shipItem that will have a parameter for item, weight
and location.
2. Set a condition for shipping fee:
- if weight is equal to 3kg set the shipping fee as P150 -
if weight is greater than and equal to 5kg set the shipping fee as P280 if weight is greater than and equal to 8kg set the shipping fee as P340
- if weight is greater than and equal to 10kg set the shipping fee as P410
if weight is greater than 10kg set the shipping fee as P560 3. Set a switch case statement that will add charge according to the location:
-
- for local, no additional fee
- for international, P250 additional fee
4. Compute the total shipping fee.
5. Return a message:
- "The total shipping fee for an <item> that will ship <location> is <total Shipping Fee>" 6. Invoke the function via console. */

function shipItem(item, weight, location) {
    
    let totalShippingFee = 0;

    if( weight <= 3) {
        totalShippingFee  = 150;
    } else if (weight <= 5){
        totalShippingFee = 280;
    } else if (weight <= 8) {
        totalShippingFee = 340;
    } else if (weight <= 10) {
        totalShippingFee = 410;
    } else if (weight > 10){
        totalShippingFee = 560;
    } else {
        return 'Please enter a valid weight value.'
    }

    switch(location.toLowerCase()) {
        case "local":
            totalShippingFee += 0;
            break;
        case "international":
            totalShippingFee += 250;
            break;
        default:
            return "Please enter a valid location"
            break;
    }

    return `The total shipping fee for an ${item} that will ship ${location} is ${totalShippingFee}.`

}

// console.log(shipItem("Banana", 10, "local"))
// console.log(shipItem("Banana", 10, "International"))
// console.log(shipItem("Banana", 6, "International"))


/* Create a simulation of adding a product in a cart using object constructors with methods and array manipulation:
1. Create an object for Product using object constructor that will have the following properties
name - String
- price Number
- quantity - Number
2. Create an object for Customer using object constructor that will have the following properties
- name - String
cart - Array of products
addToCart - method with a parameter for product that will be added
- removeToCart-method with a parameter for product that will be removed
3. In addtocart method, use an array method that will add the product to the array cart. Return a message: "<product> is added to cart."
4. In removeToCart method, use array method that will remove a specific product from the array cart. Return a message: "<product> is removed from the cart"
5. Instantiate a new product and new customer.
6. Invoke the methods of customer via console. */

class Product {
    constructor(name, price, quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
}

class Customer {
    constructor(name, cart) {
        this.name = name;

        // checks if cart is an array, if not then set it to empty array
        this.cart = Array.isArray(cart) ? cart : [];
    }

    addToCart(product) {
        this.cart.push(product);
        return `${product.name} is added to cart`
    }

    removeToCart(productName) {
        
        let foundIndex = this.cart.findIndex(item => item.name === productName);

        if(foundIndex == -1){
            return `Product with the name of ${productName} was not found.`
        }

        let removedItem = this.cart.splice(foundIndex, 1);
        return `${removedItem[0].name} was removed from the cart`
    }
}

let apple = new Product('apple', 60, 5);
let banana = new Product('banana', 10, 5);
let mango = new Product('mango', 80, 10);

//Create customer with empty cart
console.log(`Customer with empty cart`)
let ben = new Customer('ben')
console.log(ben)

// Create customer with an non-empty cart
console.log(`Customer with non-empty cart`)
let shoppingCart = [mango]
let ron = new Customer('ron', shoppingCart)
console.log(ron)

//Add products to cart, then log the customer object
console.log(`Result of adding 3 Products`)
ben.addToCart(apple)
console.log(ben)
ben.addToCart(banana)
console.log(ben)
ben.addToCart(mango)
console.log(ben)

//Removing products that are in the cart
console.log('Result of removing products in the cart')
console.log(ben.removeToCart('apple'))
console.log(ben)
console.log(ben.removeToCart('banana'))
console.log(ben)

//Removing products that are NOT in the cart
console.log(ben.removeToCart('grapes'))
console.log(ben)

